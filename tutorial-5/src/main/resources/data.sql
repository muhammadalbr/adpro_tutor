INSERT INTO soul (id, name, age, gender, occupation) VALUES (1, "Vincent", 17, "M", "Farmer");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (2, "Shizuka", 19, "F", "Priestess");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (3, "Grant", 16, "M", "Majestic Knight");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (4, "Knight 101", 17, "F", "Synthetic Knight");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (5, "Ex 73", 15, "M", "Mage");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (6, "[]", 70, "?", "???");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (7, "Jun", 20, "M", "Metal Cluster Knight");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (8, "wslhzl", 17, "M", "Majestic Knight");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (9, "olsw", 18, "F", "Synthetic Knight");
INSERT INTO soul (id, name, age, gender, occupation) VALUES (10, "bz", 19, "M", "Metal Cluster Knight");


-- INSERT INTO soul (id, age, gender, name, occupation)
-- VALUES (2, 19, 'F', 'Shizuka', 'Priestess'),
-- (3, 16, 'M', 'Grant', 'Majestic Knight'),
-- (4, 17, 'F', 'Knight 101', 'Synthetic Knight'),
-- (5, 15, 'M', 'Ex 73', 'Mage'),
-- (6, 70, '?', '[]', '???'),
-- (7, 20, 'M', 'Jun', 'Metal Cluster Knight'),
-- (8, 17, 'M', 'wslhzl', 'Majestic Knight'),
-- (9, 18, 'F', 'olsw', 'Synthetic Knight'),
-- (10, 19, 'M', 'bz', 'Metal Cluster Knight');




