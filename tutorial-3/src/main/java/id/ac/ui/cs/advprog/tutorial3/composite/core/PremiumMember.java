package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

        public String name;
        public String role;
        ArrayList<Member> children;

        public PremiumMember(String name, String role){
                this.name = name;
                this.role = role;
                children = new ArrayList<Member>();
        }

        @Override
        public String getName(){
                return this.name;
        }

        @Override
        public String getRole(){
                return this.role;
        }

        @Override
        public void addChildMember(Member member){
                if(member != null && this.role.equals("Master") || member != null && children.size() < 3){
                        this.children.add(member);
                }
        }

        @Override
        public void removeChildMember(Member member){
                this.children.remove(member);
        }

        @Override
        public List<Member> getChildMembers(){
                return this.children;
        }
}
