package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    public int getRandomIntegerBetweenRange(int min, int max){
        int x = (int) ((Math.random()*((max-min) + 1)) + min);
        return x;
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int random = getRandomIntegerBetweenRange(1, 5);
        if(weapon.getName().equals("Gun") || weapon.getName().equals("Longbow")){
            int value = weapon.getWeaponValue();
            random += value;
        }
        return random;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        String name = weapon.getName();
        return name + " upgraded";
    }
}
