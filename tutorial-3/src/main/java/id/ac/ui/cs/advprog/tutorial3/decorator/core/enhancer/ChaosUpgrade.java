package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    public int getRandomIntegerBetweenRange(int min, int max){
        int x = (int) ((Math.random()*((max-min) + 1)) + min);
        return x;
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int random = getRandomIntegerBetweenRange(50, 55);
        if(weapon.getName().equals("gaada") || weapon.getName().equals("gaada")){
            int value = weapon.getWeaponValue();
            random += value;
        }
        return random;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        String name = weapon.getName();
        return name + " upgraded";
    }
}
