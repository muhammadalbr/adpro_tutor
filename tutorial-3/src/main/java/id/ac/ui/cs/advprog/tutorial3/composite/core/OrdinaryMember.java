package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {

        public String name;
        public String role;
        ArrayList<Member> children;

        public OrdinaryMember(String name, String role){
                this.name = name;
                this.role = role;
                this.children = new ArrayList<Member>();
        }

        @Override
        public String getName(){
                return this.name;
        }

        @Override
        public String getRole(){
                return this.role;
        }

        @Override
        public void addChildMember(Member member){

        }

        @Override
        public void removeChildMember(Member member){

        }

        @Override
        public List<Member> getChildMembers(){
                return this.children;
        }
}
