package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    public int getRandomIntegerBetweenRange(int min, int max){
        int x = (int) ((Math.random()*((max-min) + 1)) + min);
        return x;
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int random = getRandomIntegerBetweenRange(10, 15);
        if(weapon.getName().equals("Longbow") || weapon.getName().equals("Sword")){
            int value = weapon.getWeaponValue();
            random =+ value;
        }
        return random;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        String name = weapon.getName();
        return name + " upgraded";
    }
}
