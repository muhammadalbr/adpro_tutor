package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    public int getRandomIntegerBetweenRange(int min, int max){
        int x = (int) ((Math.random()*((max-min) + 1)) + min);
        return x;
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int random = getRandomIntegerBetweenRange(15, 20);
        if(weapon.getName().equals("Longbow") || weapon.getName().equals("Shield") || weapon.getName().equals("Sword")){
            int value = weapon.getWeaponValue();
            random += value;
        }
        return random;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        String name = weapon.getName();
        return name + " upgraded";
    }
}
