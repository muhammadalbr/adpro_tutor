package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

        @Override
        public String getType() {
                return "Barier";
        }

        @Override
        public String defend() {
                return "Defended by Barier!";
        }
}
