package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(AdventurerRepository adventurerRepository, StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        // ToDo: Complete me
        Adventurer player = findByAlias(alias);
        if (attackType.equals("Shooter")) {
            player.setAttackBehavior(new AttackWithGun());
        } else if (attackType.equals("Witcher")) {
            player.setAttackBehavior(new AttackWithMagic());
        } else {
            player.setAttackBehavior(new AttackWithSword());
        }

        if (defenseType.equals("Armor")) {
            player.setDefenseBehavior(new DefendWithArmor());
        } else if (defenseType.equals("Barier")) {
            player.setDefenseBehavior(new DefendWithBarrier());
        } else {
            player.setDefenseBehavior(new DefendWithShield());
        }
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
