package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
        }

        @Override
        public void update() {
                if ((this.guild.getQuest().getType().equals("D")) || (this.guild.getQuest().getType().equals("R"))) {
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
